package com.actitime.www.pages;

import org.openqa.selenium.WebElement;

import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;

public class LoginHomePage extends BaseTest {
	public WebElement getUserNameField() {
		WebElement userName = elementLocator("name", "username");
		return userName;
	}

	public WebElement getPasswordField() {
		WebElement passwordValue = elementLocator("name", "pwd");
		return passwordValue;

	}
	public WebElement getLoginButton() {
		WebElement loginButton = elementLocator("id", "loginButton");
		return loginButton;
	}

	public WebElement getHeaderText() {
		WebElement headerLoginText = elementLocator("id", "headerContainer");
		return headerLoginText;
	}

	public void login(String userName, String passWord) {
		explicitWaitElementClickable(getLoginButton(), 15);
		enterText(getUserNameField(), userName);
		//explicitWaitElementClickable(getUserNameField(), 5);
		enterText(getPasswordField(), passWord);
		clickElement(getLoginButton());
	}
}
