package com.ActiTime.www;

import java.util.HashMap;
import java.util.Set;

import org.testng.annotations.Test;

import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;

public class UserLocationsVerify extends BaseTest{
	@Test (groups= {"Smoke"}, priority=1)
	public void userLocVerifyHashMap() throws Exception {
		/*[u1, l1
		 u2, l2
		 u3, l3]
		 Length of the two dimensional array is 3*/
		HashMap<Object, Object> mapUserLoc = new HashMap<Object, Object>();
		
		Object[][] userLocData = readExcelWorksheet("src\\main\\resources\\TestData\\UserLocationDetails.xlsx", "UserLocSheet");
		int numOfRowsNumUserLocData = userLocData.length;
		
		for (int i=0; i < numOfRowsNumUserLocData; i++) {
		
			/*String user = userLocData[i][0].toString();
			String userLocation = userLocData[i][1].toString();*/
			
			Object user = userLocData[i][0];
			Object userLocation = userLocData[i][1];
			mapUserLoc.put(user, userLocation);
		}
				
	    System.out.println( "map = " + mapUserLoc );
	    Object value = mapUserLoc.get("Barber, Robert");
	    System.out.println( "locationName = " + value);
	    System.out.println(mapUserLoc.keySet());
	   Set<Object> allUsers= mapUserLoc.keySet();
	   for(Object eachUser:allUsers)
	   {
		   Object timeZone = mapUserLoc.get(eachUser);
		    System.out.println( "locationName = " + timeZone);
		    
	   }
	    
	    
		}
}
	

