package com.ActiTime.www;




import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.Test;

import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;
import com.relevantcodes.extentreports.LogStatus;


public class AppTest extends BaseTest {
	
	// initializating the logger constructor  
	final static Logger logger = Logger.getLogger(AppTest.class);

	// @Test when it is used, main method can be removed and @Test will be used as main 
	//@Test (priority=1)(groups= {"Smoke"})
	@Test (groups= {"Smoke"}, priority=1)
	public void openActiTimeApp() throws InterruptedException, FileNotFoundException, IOException { 
		logger.info("Starting of "+new Object(){}.getClass().getEnclosingMethod().getName() +"test case");
		openURL("https://demo.actitime.com");
			sleep(2000);
		logger.info("actitime url opened");
		logger.info(new Object(){}.getClass().getEnclosingMethod().getName() +"test case is pass");
		//System.out.println(driver);
		test.log(LogStatus.PASS, "Demo actitime url test case passed");
	}
	//@Test (priority=2)
	//@Test //(dependsOnMethods = {"openActiTimeApp"})
	//@Test (groups= {"Regression"}, priority=2)
	public void maximizeActiTimeApp() throws InterruptedException { 
		openURL("https://demo.actitime.com");
		browserMaximize();
		sleep(2000);
	}

	
}