package com.ActiTime.www;

import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.actitime.www.pages.LoginHomePage;
import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;
import com.relevantcodes.extentreports.LogStatus;

public class DataProviderDemo extends BaseTest{
	final static Logger logger = Logger.getLogger(DataProviderDemo.class);
	LoginHomePage loginhomepage = new LoginHomePage();
	@DataProvider
	public Object[][] getDataFromSource() throws Exception {
		Object[][] loginData  = readExcelWorksheet("src\\main\\resources\\TestData\\LoginCredentials.xlsx", "LoginSheet");
		return loginData;	
	}
	
	@Test(dataProvider = "getDataFromSource", groups= {"Smoke", "Regression"}, priority=1)
	//@Test(dataProvider = "getDataFromSource")
	public void loginActitime(String scenarioName, String uid, String pwd) {
		openURL("https://demo.actitime.com");
		browserMaximize();
		loginhomepage.login(uid, pwd);
		logger.info(scenarioName + " is executed as part of the dataprovider demo");
		test.log(LogStatus.PASS, scenarioName + " is executed as part of the dataprovider demo");
	}
	
}
