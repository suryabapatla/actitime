package com.ActiTime.www;

//import java.util.logging.Logger; 
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.actitime.www.pages.LoginHomePage;
import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;

public class Login extends BaseTest {
	
	final static Logger logger = Logger.getLogger(Login.class);
	LoginHomePage loginhomepage = new LoginHomePage();
	
	@Test (groups= {"Smoke", "Regression"}, priority=1)
	public void managerLogin() throws Exception { 
	openURL("https://demo.actitime.com");
	browserMaximize();
	Object managerUserName = readExcelCell("src\\main\\resources\\TestData\\LoginCredentials.xlsx", "LoginSheet", 1,1 );
	Object managerPassword = readExcelCell("src\\main\\resources\\TestData\\LoginCredentials.xlsx", "LoginSheet", 1,2 );
	loginhomepage.login(managerUserName.toString(), managerPassword.toString());
	logger.info("manager log in is successfull");
	//logger.error(message);
	
	}
	
	@Test (groups= {"Smoke", "Regression"}, priority=1)
	public void userLogin() throws Exception { 
	openURL("https://demo.actitime.com");
	browserMaximize();
	Object userName = readExcelCell("src\\main\\resources\\TestData\\LoginCredentials.xlsx", "LoginSheet", 2,1 );
	Object userPassword = readExcelCell("src\\main\\resources\\TestData\\LoginCredentials.xlsx", "LoginSheet", 2,2 );
	loginhomepage.login(userName.toString(), userPassword.toString());
	}
	
	
}
